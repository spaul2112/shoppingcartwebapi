﻿using shoppingCart.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.ViewModel
{
    public class OrderViewModel
    {
		public int Id { get; set; }
		public string OrderNumber { get; set; }
		public ICollection<OrderItemViewModel> Items { get; set; }
		public string CustomerName { get; set; }
		public string CustomerAddress1 { get; set; }
		public string CustomerAddress2 { get; set; }
		public string CustomerCity { get; set; }
		public string CustomerCounty { get; set; }
		public string CustomerCountry { get; set; }
	}
}
