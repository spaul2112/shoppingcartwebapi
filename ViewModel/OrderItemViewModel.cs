﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.ViewModel
{
    public class OrderItemViewModel
    {
		public int Quantity { get; set; }
		public int ProductId { get; set; }
		public string ProductName { get; set; }
		public decimal ProductUnitPrice { get; set; }
		public decimal ProductUnitWeight { get; set; }
	}
}
