﻿using Microsoft.AspNetCore.Mvc;
using shoppingCart.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.Controllers
{
	[Route("api/[Controller]")]
    public class ProductsController: Controller
    {
        public ProductsController(ShoppingCartRepository repository)
		{
			_repository = repository;
		}

		private readonly ShoppingCartRepository _repository;

		[HttpGet]
		public IActionResult Get()
		{
			return Ok(_repository.GetAllProducts());
		}
	}
}
