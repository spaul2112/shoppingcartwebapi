﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using shoppingCart.Data;
using shoppingCart.Data.Entities;
using shoppingCart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.Controllers
{
	[Route("api/[Controller]")]
	public class OrdersController: Controller
    {
		public OrdersController(ShoppingCartRepository repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		private readonly ShoppingCartRepository _repository;
		private readonly IMapper _mapper;

		public IActionResult Get()
		{
			return Ok(_mapper.Map<IEnumerable<Order>, IEnumerable<OrderViewModel>>(_repository.GetAllOrders()));
		}

		[HttpGet("{id:Int}")]
		public IActionResult Get(int id)
		{
			var order = _repository.GetOrderById(id);
			if (order == null)
			{
				return NotFound();
			}
			else
			{
				return Ok(_mapper.Map<Order,OrderViewModel>(order));
			}
		}

		[HttpPost()]
		public IActionResult Post([FromBody]OrderViewModel model)
		{
			var newModel = _mapper.Map<OrderViewModel, Order>(model);
			newModel.Items.ToList().ForEach(items => items.Product = null);
			if (_repository.SaveOrder(newModel))
			{
				
				return Created($"/api/orders/{newModel.Id}", _mapper.Map<Order, OrderViewModel>(_repository.GetOrderById(newModel.Id)));
			}
			else
			{
				return BadRequest("Failed to save new order");
			}
		}
	}
}
