﻿using AutoMapper;
using shoppingCart.Data.Entities;
using shoppingCart.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.Data
{
    public class ShoppingCartMappingProfile: Profile
    {
		public ShoppingCartMappingProfile()
		{
			CreateMap<Order, OrderViewModel>()
				.ReverseMap();
			CreateMap<OrderItem, OrderItemViewModel>()
				.ReverseMap();
		}
 
    }
}
