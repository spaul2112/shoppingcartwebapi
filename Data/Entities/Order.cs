﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.Data.Entities
{
	public class Order
	{
		public int Id { get; set; }
		public string OrderNumber { get; set; }
		public ICollection<OrderItem> Items { get; set; }
		public Customer customer { get; set; }
		public int CustomerId {get; set;}
	}
}
