﻿using Microsoft.EntityFrameworkCore;
using shoppingCart.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.Data
{
    public class ShoppingCartRepository
    {
		private readonly ShoppingCartContext _context;
		public ShoppingCartRepository(ShoppingCartContext context)
		{
			_context = context;
		}

		public IEnumerable<Product> GetAllProducts()
		{
			return _context.Products
					.OrderBy(p => p.Name)
					.ToList();
		}

		public IEnumerable<Order> GetAllOrders()
		{
			return _context.Orders
				.Include(o => o.customer)
				.Include(i => i.Items)
				.ThenInclude(k => k.Product)
				.ToList();
		}

		public Order GetOrderById(int orderId)
		{
			return _context.Orders
				.Include(o => o.customer)
				.Include(i => i.Items)
				.ThenInclude(k => k.Product)
				.Where(k => k.Id == orderId)
				.FirstOrDefault();
		}

		public bool SaveOrder(Order model)
		{
			_context.Add(model);
			return _context.SaveChanges() > 0;

		}

	}
}
