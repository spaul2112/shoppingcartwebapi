﻿using Microsoft.EntityFrameworkCore;
using shoppingCart.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace shoppingCart.Data
{
    public class ShoppingCartContext : DbContext
    {   public DbSet<Customer> Customers { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Product> Products { get; set; }

		public ShoppingCartContext(DbContextOptions<ShoppingCartContext> options): base(options)
		{

		}
    }
}
